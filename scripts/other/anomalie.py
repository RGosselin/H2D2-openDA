#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import datetime
import os
import sys
import numpy as np

tini = {
       'rdps'  : 1472688600,   # 2016-09-01 00:00:00
       'hrdps' : 1477959000,   # 2016-11-01 00:00:00
       }
tfin = {
       'rdps'  : 1483142400,   # 2016-12-31 00:00:00
       'hrdps' : 1479686400,   # 2016-11-20 24:00:00
       }
tstp = 5*60         # 5'

def mean(numbers):
    return float(sum(numbers)) / max(len(numbers), 1)

def read_interpole(fnam, tini, tfin, tstp):
    data = []
    tcur = tini
    tlim = tfin + tstp*0.5
    with open(fnam, 'rt') as ifile:
        ilin = 0
        reader = csv.reader(ifile, delimiter=';')
        t1 = None
        for r2 in reader:
            ilin += 1
            t2 = int(float(r2[0]))
            #print t2, tini, tfin
            if tini < t2 < tfin:
                if not t1: t1, r1 = t2, r2
                tsup = min(t2, tlim)
                while tcur < tsup:
                    eta = float(tcur-t1) / float(t2-t1)
                    toks = [tcur]
                    for v1_, v2_ in zip(r1[1:], r2[1:]):
                        if not v1_: break
                        if not v2_: break
                        v1 = float(v1_)
                        v2 = float(v2_)
                        v  = v1*(1.0-eta) + eta*v2
                        toks.append(v)
                    tcur += tstp
                    data.append(toks)
            t1 = t2
            r1 = r2
    #print fnam, data[0][0], data[-1][0]
    return data

def exec_one(fsim, fmoy, fano, tini, tfin, tstp):    
    simdata = read_interpole(fsim, tini, tfin, tstp)

    hw = 7*24*3600 / tstp       # half window
    w = np.ones(hw*2+1,'d')
    w = w / w.sum()

    simarray = np.array(simdata)
    simmean  = np.convolve(simarray[:,0], w, 'valid')
    for i in xrange(1, simarray.shape[1]):
        simmean = np.c_[simmean, np.convolve(simarray[:,i], w, 'valid') ]

    with open(fano, 'wt') as ifout:
        nlin, ncol = simmean.shape
        for im in xrange(nlin):
            id = im + hw
            ts = int(simarray[id,0] + 0.5)
            tm = int(simmean [im,0] + 0.5)
            assert int(ts) == int(tm)
            simd = simarray[id,1:]
            simm = simmean [im,1:]
            ifout.write('%6i %3i %12i\n' % (ncol-1, 1, ts))
            for sd, sm in zip(simd, simm):
                ifout.write('%12.6f\n' % (sd-sm))

    with open(fmoy, 'wt') as ifout:
        nlin, ncol = simmean.shape
        for im in xrange(nlin):
            tm = int(simmean [im,0] + 0.5)
            simm = simmean [im,1:]
            ifout.write('%6i %3i %12i\n' % (ncol-1, 1, tm))
            for sm in simm:
                ifout.write('%12.6f\n' % (sm))

def main():
    kind   = sys.argv[1]
    subdir = ''
    fsim = 'simul000.pst.prb.csv'
    fano = 'anomalie-%s-%s.prb' % (subdir, kind)
    fmoy = 'momobile-%s-%s.prb' % (subdir, kind)
    print 'Process  %s' % fsim
    print 'Generate %s' % fano
    print 'Generate %s' % fmoy
    exec_one(fsim, fmoy, fano, tini[kind], tfin[kind], tstp)
    
main()