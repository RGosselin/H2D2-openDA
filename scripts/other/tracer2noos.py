#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import argparse

def tracer2noos(inputname, outputname):

    finp = inputname
    fout = outputname
    iteration = 0
    time_int = 0
    
    count_conv = 0
    count_noconv = 0
    
    with open(finp, 'rt') as ifs, open(fout, 'wt') as ofs:
        while True:
            l = ifs.readline()
            if not l:
                for x in range(0, 24):
                    ofs.write( '20100101%04d %f\n' % (x*100, wallTimeS*(2-float(count_conv)/(float(count_conv)+float(count_noconv)))))
                break
            else:
                prev_iteration = iteration
                prev_time = time_int
                walltime, epoch, N1, N2, N3, solver, iteration, res = l.split()            #    reads line splits
                
                epoch, iteration, res = float(epoch), int(iteration), float(res)        #    converts strings to number of lines, columns and epoch time
                strtimes = time.strftime("%Y%m%d%H%M", time.gmtime(epoch))                #    converts epoch time to YYYYmmddHHMM format
                time_int = int(strtimes)                                                #    stores date in temp container          
                
                days, hours, minutes, seconds = walltime.split(':')
                wallTimeS = float(seconds) + 60*float(minutes)+ 60*60*float(hours) + 24*60*60*float(days)
                
                if (time_int != prev_time):
                    if (solver == "nwtn"): 
                        if iteration == 1:
                            if prev_iteration == 20:
                                count_noconv = count_noconv + 1
                            else:
                                count_conv = count_conv + 1

    print("Total walltime is {}".format(walltime))
    print("Total walltime in seconds is {}".format(wallTimeS))
    print("Convergence correction factor is {}".format((2-float(count_conv)/(float(count_conv)+float(count_noconv)))))
    

parser = argparse.ArgumentParser(description='Reads and converts a tracer log file walltime noos file.')
parser.add_argument("input", type=str, help='input tracer filename')
parser.add_argument("output", type=str, help='output name')

args = parser.parse_args()

if __name__ == "__main__":
    tracer2noos(args.input,args.output)
        


