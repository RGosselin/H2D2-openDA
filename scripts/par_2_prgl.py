#!/usr/bin/env python
"""Reads and converts an OpenDA parameter file to a H2D2 .prgl file.
Only the "par_bXXX" parameter values are used by this script
Input arguments are:
    - input filename
    - output filename
Please modify the script to change fixed prgl values and select which prgl
values are replaced by the one from the parameter file.
"""
# -*- coding: utf-8 -*-

import math
import argparse
from scipy.special import erfinv

def par2prgl(inputName, outputName, prgl):
    # Create empty list
    prglValue = []

    # Open parameter file
    with open(inputName, 'rt') as ifs:
        # Loop on all lines
        while True:
            l = ifs.readline()
            # If line does not exist, break loop
            if not l:
                break
            else:
                # If the line is the number of parameters
                if l[0:4] == 'Npar':
                    try:
                        _, nPar = l.split('=')
                        # Check if the number of parameters is 5
                        # If not, break loop and return an error message
                        if float(nPar) != 5:
                            print('Error, check the number of OpenDA parameters!')
                            break
                    except ValueError:
                        nPar = 0
                # Else if the line starts with par001
                elif l[0:6] == 'par001':                                                                    #   hmin
                    # Read parA and parB values
                    _, _, valueBTemp = l.split(',')
                    # Lower limit (asymptot)
                    L1 = 0.0
                    # Upper limit (asymptot)
                    L2 = 0.5
                    # Parameter standard deviation
                    sigma = 0.1
                    # Initial parameter value
                    valIni = 0.1
                    # Compute new value using CDG (-infinity -> L1, +infinity -> L2)
                    mu = valIni - sigma*math.sqrt(2)*erfinv(2*(valIni-L1)/(L2-L1)-1)
                    valueBTempCDF = 0.5 * (1 + math.erf((float(valueBTemp)-mu)/(sigma*math.sqrt(2))))
                    # Store temp value in prglValue container
                    prglValue.append((L2-L1)*valueBTempCDF+L1)
                elif l[0:6] == 'par002':                                                                    #   htrig/hmin
                    _, _, valueBTemp = l.split(',')
                    L1 = 1.1
                    L2 = 5
                    sigma = 0.1
                    valIni = 2.0
                    mu = valIni - sigma*math.sqrt(2)*erfinv(2*(valIni-L1)/(L2-L1)-1)
                    valueBTempCDF = 0.5 * (1 + math.erf((float(valueBTemp)-mu)/(sigma*math.sqrt(2))))
                    prglValue.append((L2-L1)*valueBTempCDF+L1)
                elif l[0:6] == 'par003':                                                                    #   manning
                    _, _, valueBTemp = l.split(',')
                    L1 = 0.05
                    L2 = 1.0
                    sigma = 0.1
                    valIni = 0.5
                    mu = valIni - sigma*math.sqrt(2)*erfinv(2*(valIni-L1)/(L2-L1)-1)
                    valueBTempCDF = 0.5 * (1 + math.erf((float(valueBTemp)-mu)/(sigma*math.sqrt(2))))
                    prglValue.append((L2-L1)*valueBTempCDF+L1)
                elif l[0:6] == 'par004':                                                                    #   Peclet
                    _, _, valueBTemp = l.split(',')
                    L1 = 0.4
                    L2 = 1.0
                    sigma = 0.1
                    valIni = 0.5
                    mu = valIni - sigma*math.sqrt(2)*erfinv(2*(valIni-L1)/(L2-L1)-1)
                    valueBTempCDF = 0.5 * (1 + math.erf((float(valueBTemp)-mu)/(sigma*math.sqrt(2))))
                    prglValue.append((L2-L1)*valueBTempCDF+L1)
                elif l[0:6] == 'par005':                                                                    #   Darcy
                    _, _, valueBTemp = l.split(',')
                    L1 = 0.0
                    L2 = 2.0
                    sigma = 0.1
                    valIni = 1.0
                    mu = valIni - sigma*math.sqrt(2)*erfinv(2*(valIni-L1)/(L2-L1)-1)
                    valueBTempCDF = 0.5 * (1 + math.erf((float(valueBTemp)-mu)/(sigma*math.sqrt(2))))
                    prglValue.append((L2-L1)*valueBTempCDF+L1)

    # Open outputfile
    with open('%s' % (outputName), 'wt') as ofs:
        ofs.write('%i %i %15.5f\n' % (1, len(prgl), 0))

        # Loop on prgl values.
        for iz in range(0, len(prgl)):
            # If prgl value is dw_threshold
            if prgl[iz] == 'dw_threshold':
                # Print and write value = hmin * htrig/hmin (Second OpenDA
                # parameter is htrig / hmin instead of simply htrig)
                print(('dw_threshold = {}'.format(prglValue[0]*prglValue[1])))
                ofs.write('%9.6e ' % (prglValue[0]*prglValue[1]))
            # else if value is dw_min
            elif prgl[iz] == 'dw_min':
                # Print and write hmin value (1st OpenDA parameter)
                print(('dw_min = {}'.format(prglValue[0])))
                ofs.write('%9.6e ' % (prglValue[0]))
            # else if value is dw_manning
            elif prgl[iz] == 'dw_manning':
                # Print and write parameter #3 value
                print(('dw_manning = {}'.format(prglValue[2])))
                ofs.write('%9.6e ' % (prglValue[2]))
            # else if value is dw_peclet
            elif prgl[iz] == 'dw_peclet':
                # Print and write parameter #4 value
                print(('dw_peclet = {}'.format(prglValue[3])))
                ofs.write('%9.6e ' % (prglValue[3]))
             # else if value is dw_darcy
            elif prgl[iz] == 'dw_darcy':
                # Print and write parameter #5 value
                print(('dw_darcy = {}'.format(prglValue[4])))
                ofs.write('%9.6e ' % (prglValue[4]))
            # else write value from the main() prgl definition
            else:
                ofs.write('%s ' % prgl[iz])

        ofs.write('\n')

    print('Conversion completed!')

def main():
    """Reads and converts an OpenDA parameter file to a H2D2 .prgl file.
    Only the "par_bXXX" parameter values are used by this script
    Input arguments are:
        - input filename
        - output filename
    Please modify the script to change fixed prgl values and select which prgl
    values are replaced by the one from the parameter file.
    """
    parser = argparse.ArgumentParser(description='Reads and converts parameters from OpenDA to H2D2 global properties file.')
    parser.add_argument("input", type=str, help='input filename')
    parser.add_argument("output", type=str, help='output filename')

    args = parser.parse_args()

    prgl = []

    prgl.append('9.8059999999999992e+000')                #  1 gravity
    prgl.append('4.2000000000000000e+001')                #  2 latitude
    prgl.append('6')                                      #  3 cw formula ! 6 - Kumar old waves / -1 - Woo relative
    prgl.append('1.0000000000000000e-006')                #  4 laminar viscosity
    prgl.append('1.0000000000000000e+000')                #  5 mixing length coef
    prgl.append('0.0000000000000000e+000')                #  6 mixing length coef related to grid size
    prgl.append('1.0000000000000000e-006')                #  7 viscosity limiter: inf
    prgl.append('1.0000000000000000e+002')                #  8 viscosity limiter: sup
    prgl.append('dw_threshold')                           #  9 dry/wet: threshold depth
    prgl.append('dw_min')                                 # 10 dry/wet: min depth
    prgl.append('dw_manning')                             # 11 dry/wet: manning
    prgl.append('1.0000000000000000e-000')                # 12 dry/wet: |u| max
    prgl.append('1.0000000000000000e+000')                # 13 dry/wet: porosity
    prgl.append('0.0000000000000000e+000')                # 14 dry/wet: damping
    prgl.append('1.0')                                    # 15 dry/wet: coef. convection
    prgl.append('1.0')                                    # 16 dry/wet: coef. gravity
    prgl.append('1.0e-003')                               # 17 dry/wet: nu visco
    prgl.append('dw_peclet')                              # 18 dry/wet: Peclet
    prgl.append('dw_darcy')                               # 19 dry/wet: nu darcy
    prgl.append('1.0000000000000000e+000')                # 20 peclet           -> 1.2
    prgl.append('0.0e-004')                               # 21 damping coef.    -> 0.0
    prgl.append('0.0e-000')                               # 22 free surface smoothing (Darcy)
    prgl.append('0.2500000000000000e-005')                # 23 free surface smoothing (Lapidus)
    prgl.append('1.0')                                    # 24 coef. convection
    prgl.append('1.0')                                    # 25 coef. gravity
    prgl.append('1.0')                                    # 26 coef. manning
    prgl.append('0.0')                                    # 27 coef. wind
    prgl.append('001')                                    # 28 coef. boundary integral
    prgl.append('1.0e-018')                               # 29 coef. penality
    prgl.append('1.0e-007')                               # 30 coef. Kt perturbation (~ 0.1*sqrt(eps_mach); eps_mach ~ 2.2e-16)
    prgl.append('1.0e-009')                               # 31 min   Kt perturbation (~ 0.1*sqrt(eps_mach); eps_mach ~ 2.2e-16)
    prgl.append('1.0')                                    # 32 coef. Kt relaxation
    prgl.append('1')                                      # 33 flag prel perturbation (0 = false, #=0 = true)
    prgl.append('1')                                      # 34 flag prno perturbation (0 = false, #=0 = true)

    par2prgl(args.input, args.output, prgl)

    if args.output:
        print(("output filename is {}".format(args.output)))

if __name__ == "__main__":
    main()
