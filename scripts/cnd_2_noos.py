#!/usr/bin/env python
"""Reads and converts a .cnd file to N .noos files.
Input arguments are:
    - input filename
    - -o=output basename (optional, the inputfile name will be used if
                          not provided)
The .noos files have the location and type of the boundary in the header
One .noos file is written per location. 'XXX.noos' is added to the basename
Date format in the output files is YYYYmmddHHMM
"""
# -*- coding: utf-8 -*-

import time
import argparse

def cndOutputListGenerator(inputName, outputName):
    """Reads the first line of a .cnd file to determine the number of necessary
    .noos files.
    Creates the corresponding filename (basename + XXX + .noos), opens the file
    and returns the list of open files.
    """
    # creates empty list of output files
    outputList = []

    # Open .cnd input file to create a list of output filenames
    with open(inputName, 'rt') as ifs:
        #   read line
        line = ifs.readline()
        # Read line splits
        nLin, times, _ = line.strip().split(None, 2)
        # Convert strings to number of lines (int) and epoch time (float)
        nLin, times = int(nLin), float(times)
        #   creates the list of output filenames
        for iw in range(nLin):
            # output filename = basename + XXX + .noos
            filename = outputName + str(iw+1).zfill(3) + '.noos'
            # open output file
            fileoutput = open(filename, 'wt')
            # add output file to the list
            outputList.append(fileoutput)

    return(outputList)

def cnd2noos(inputName, outputList):
    """Converts a single .cnd file (H2D2 format) to N .noos files.
    Date format in .noos files is YYYYmmddHHMM
    The location (variable name) and type of the condition is written at the
    beginning of the .noos file.
    """
    # Open .cnd file
    with open(inputName, 'rt') as ifs:
        # Create outputfile header toggle list filled with False value
        header = [False]*len(outputList)
        # Loop on all inputfile lines
        while True:
            line = ifs.readline()
            # If line does not exist, break loop
            if not line:
                break
            else:
                # Read line splits
                nLin, times, _ = line.strip().split(None, 2)
                # Convert strings to number of lines (int)
                # and epoch time (float)
                nLin, times = int(nLin), float(times)
                # Convert epoch time to YYYYmmddHHMM format
                strTimeOut = time.strftime("%Y%m%d%H%M", time.gmtime(times))
                # Store date in temporary container
                timeOut = int(strTimeOut)
                # Loop on N variables / N output files
                for iv in range(nLin):
                    cndLocation, cndType, cndValue = \
                        ifs.readline().strip().split()
                    # If header as not been written yet (value = False)
                    if not header[iv]:
                        # Write location
                        outputList[iv].write('# Location : %s\n' % cndLocation)
                        # And boundary type
                        outputList[iv].write('# Type : %s\n' % cndType)
                        # Change header toggle to True
                        header[iv] = True
                    # Write time and condition value
                    outputList[iv].write('%i %s\n' % (timeOut, cndValue))

    print('Conversion completed!')

def closeOutputList(fileList):
    """Closes all open files in the list provided
    """
    for filename in fileList:
        filename.close()

def main():
    """Reads and converts a .cnd file to N .noos files.
    Input arguments are:
        - input filename
        - -o=output basename (optional, the inputfile name will be used if
                              not provided)
    The .noos files have the location and type of the boundary in the header
    One .noos file is written per location. 'XXX.noos' is added to the basename
    Date format in the output files is YYYYmmddHHMM
    """
    parser = argparse.ArgumentParser(description='Reads and converts \
                                     a .cnd files to N (number of conditions) \
                                     .noos files.')
    parser.add_argument("input",
                        type=str,
                        help='input H2D2 condition filename')
    parser.add_argument("-o",
                        "--output",
                        type=str,
                        help='output basename')

    args = parser.parse_args()
    print("input filename is {}".format(args.input))

    # If the output basename is provided, use it
    if args.output:
        # Generate output files list
        outputList = cndOutputListGenerator(args.input, args.output)
        # Convert .cnd to .noos
        cnd2noos(args.input, outputList)
        # Close output files
        closeOutputList(outputList)
    # Otherwise, use the input filename as basename for the output files
    else:
        outputList = cndOutputListGenerator(args.input, args.input)
        cnd2noos(args.input, outputList)
        closeOutputList(outputList)

    if args.output:
        print("output filename(s) is(are) {}XXX.noos".format(args.output))

if __name__ == "__main__":
    main()
    