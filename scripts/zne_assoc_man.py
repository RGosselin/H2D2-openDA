#!/usr/bin/env python
"""Reads a nodal value file, and generates an association file based on
the nodal values.
The script writes a file with N columns (one for each zone), and fills the
columns with 0 or 1 depending on the zone associated with the node.
Input arguments are:
    - Nodal values filename
    - Association filename (output)

The example provided here writes 5 zones:
    - manning < 0.03
    - 0.03 <= manning < 0.05
    - 0.05 <= manning < 0.1
    - 0.1 <= manning < 0.15
    - 0.15 <= manning
It can easily modified to change the number of zones (nbZones) and the criteria
Values from the nodal value file are stored in the tuple vnod[a][b], where a is
the column number in the nodal value file, and b the node number.
"""
# -*- coding: utf-8 -*-

import argparse

def read_header(file):
    """Simple function to read the header of a H2D2 data file.
    Input is an open Python file.
    Returns the tuple (number of nodes, number of columns)
    """
    # Read first line of the input file
    line = file.readline()
    # Extract the number of nodes (1st number) and number of columns (2nd number)
    nbNodes, nbCol, _ = line.strip().split(None, 2)
    # Convert strings to integers
    nbNodes = int(nbNodes)
    nbCol = int(nbCol)
    # Return the value
    return (nbNodes, nbCol)

def read_vnod(vnodFile):
    """Read H2D2 data files and stores the values in the vnod list.
    Each column in the data file is a sublist of vnod
    """
    # Open input file
    with open(vnodFile, 'rt') as afs:
        # Call header function to extract the number of columns
        header = read_header(afs)
        nbColumns = header[1]
        # Create empty nodal values list
        vnod = []
        # Create sublists - one for each column
        for i in range(0, nbColumns):
            vnod.append([])
        # Store nodal values in vnod
        for line in afs:
            value = line.strip().split(' ')
            # Loop on columns
            for i in range(0, nbColumns):
                vnod[i].append(float(value[i]))
    # Return vnod
    return vnod

def compute_zne(vnod, outputfile):
    """Write the association file depending on the nodal value
    """
    # Read number of nodes and dimensions from the nodal value list dimensions
    nbNodes = len(vnod[0])
    dimVnod = len(vnod)
    # Number of zones
    nbZones = 5
    print(('Number of nodes = %i' % nbNodes))
    print(('Nodal values - number of columns = %i' % dimVnod))
    print(('Number of zones = %i' % nbZones))

    # Create output file
    with open(outputfile, 'wt') as ofs:
        # Write the header (H2D2 data format)
        ofs.write('%i %i 0.0\n' % (nbNodes, nbZones))

        # Loop on all nodes
        for i in range(0, nbNodes):
            # If the nodal value of the first dimension (0) is < 0.03
            if vnod[0][i] < 0.03:
                # Associate node with zone #1
                zone1 = 1
                zone2 = 0
                zone3 = 0
                zone4 = 0
                zone5 = 0
            # Else if the value is < 0.05
            elif vnod[0][i] < 0.05:
                # Associate node with zone #2
                zone1 = 0
                zone2 = 1
                zone3 = 0
                zone4 = 0
                zone5 = 0
            # Else if the value is < 0.1
            elif vnod[0][i] < 0.1:
                # Associate node with zone #3
                zone1 = 0
                zone2 = 0
                zone3 = 1
                zone4 = 0
                zone5 = 0
            # Else if the value is < 0.15
            elif vnod[0][i] < 0.15:
                # Associate node with zone #4
                zone1 = 0
                zone2 = 0
                zone3 = 0
                zone4 = 1
                zone5 = 0
            # If the nodal value does not meet the previous requirements
            else:
                # Associate node with zone #5
                zone1 = 0
                zone2 = 0
                zone3 = 0
                zone4 = 0
                zone5 = 1
            # Write the line with the zone association values
            ofs.write('%i %i %i %i %i\n' % (zone1, zone2, zone3, zone4, zone5))

def main():
    """Reads a nodal value file, and generates an association file based on
    the nodal values.
    The script writes a file with N columns (one for each zone), and fills the
    columns with 0 or 1 depending on the zone associated with the node.
    Input arguments are:
        - Nodal values filename
        - Association filename (output)
    """
    parser = argparse.ArgumentParser(description='Reads %1 original nodal \
                                     values file, write %2 zone association\
                                     file')
    parser.add_argument("orig_file",
                        type=str,
                        help='original nodal values filename')
    parser.add_argument("asso_file",
                        type=str,
                        help='zone association filename')

    args = parser.parse_args()

    # Call read_vnod to extract nodal values from data file
    vnod = read_vnod(args.orig_file)
    # Call compute_zne to associate nodes with zones
    compute_zne(vnod, args.asso_file)

if __name__ == '__main__':
    main()
