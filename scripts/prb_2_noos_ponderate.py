#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import argparse

def prb2noos(inputname, outputname, windxname, windyname, obsname):

    finp = inputname
    fout = outputname
    
    windx = windxname
    windy = windyname
    obs = obsname
    
    windOccurence = []
    nNorm = []
    nTot = []
    
    with open(finp, 'rt') as ifs:
        l = ifs.readline()
        nlin, ncol, times = l.split()                                            #    reads line splits
        nlin = int(nlin)
        for iw in range(nlin):
            windOccurence.append([])
            nNorm.append(0)
            nTot.append(0)
            for i in range(25):
                windOccurence[iw].append(0)

    with open(windx, 'rt') as ifsx, open(windy, 'rt') as ifsy:
        while True:
            lx = ifsx.readline()
            ly = ifsy.readline()
            if not lx:
                break
            else:
                nlin, ncol, times = lx.split()                                            #    reads line splits
                nlin, ncol, times = int(nlin), int(ncol), float(times)                    #    converts strings to number of lines, columns and epoch time
                
                for iv in range(nlin):
                    wx = float(ifsx.readline().strip())
                    wy = float(ifsy.readline().strip())
                    windMag = (wx**2+wy**2)**0.5
                    
                    for i in range(25):
                        if (windMag < i+1):
                            windOccurence[iv][i] += 1
                            break      

        for iv in range(nlin):
            for i in range(25):
                if (windOccurence[iv][i]>0):
                    nNorm[iv] += 1
                    nTot[iv] += windOccurence[iv][i]
            print(nNorm[iv])
            print(nTot[iv])
  
    timeObs = []
    valueObs = []
    

    
    with open(obs, 'rt') as ifs:
        l = ifs.readline()
        nlin, ncol, times = l.split()                                            #    reads line splits
        nlin, ncol, times = int(nlin), int(ncol), float(times)                    #    converts strings to nuber of lines, columns and epoch time
        for iw in range(nlin):
            valueObs.append([])
    
    with open(obs, 'rt') as ifs:
        while True:
            l = ifs.readline()
            if not l:
                break
            else:
                nlin, ncol, times = l.split()                                            #    reads line splits
                nlin, ncol, times = int(nlin), int(ncol), float(times)                    #    converts strings to nuber of lines, columns and epoch time
                strtimes=time.strftime("%Y%m%d%H%M", time.gmtime(times))                #    converts epoch time to YYYYmmddHHMM format
                timeObs.append(int(strtimes))                                            #    stores date in temp container
                
                for iv in range(nlin):
                    l = float(ifs.readline().strip())
                    valueObs[iv].append(l)                       
                            
    ofs = []
    with open(finp, 'rt') as ifs:
        l = ifs.readline()
        nlin, ncol, times = l.split()                                            #    reads line splits
        nlin, ncol, times = int(nlin), int(ncol), float(times)                    #    converts strings to nuber of lines, columns and epoch time
        for iw in range(nlin):
            fileoutput=open('%s%02i%s' % (fout,iw+1,'.noos'), 'wt')
            ofs.append(fileoutput)

    with open(finp, 'rt') as ifs, open(windx, 'rt') as ifsx, open(windy, 'rt') as ifsy:
        while True:
            l = ifs.readline()
            lx = ifsx.readline()
            ly = ifsy.readline()
            if not l:
                break
            else:
                nlin, ncol, times = l.split()                                            #    reads line splits
                nlin, ncol, times = int(nlin), int(ncol), float(times)                    #    converts strings to nuber of lines, columns and epoch time
                strtimes=time.strftime("%Y%m%d%H%M", time.gmtime(times))                #    converts epoch time to YYYYmmddHHMM format
                time_int = int(strtimes)                                                #    stores date in temp container
                
                ix = 0
                while ix < len(timeObs):
                    if (timeObs[ix] == time_int):
                        indexTemp = ix
                        break
                    ix +=1
                
                for iv in range(nlin):
                    l = float(ifs.readline().strip())
                    wx = float(ifsx.readline().strip())
                    wy = float(ifsy.readline().strip())
                    windMag = (wx**2+wy**2)**0.5
                    
                    errPond = 0
                    for i in range(25):
                        if (windMag < i+1):
                            errPond = (l-valueObs[iv][indexTemp])*nTot[iv]/(nNorm[iv]*windOccurence[iv][i])
                            break
                    
                    ofs[iv].write('%i %16.10e\n' % (time_int,errPond) )
        
    for of in ofs:
        of.close
        
    print('Conversion completed!')    
    
        

parser = argparse.ArgumentParser(description='Reads and converts a .prb files to N (number of probes) .noos files.')
parser.add_argument("input", type=str, help='input .prb filename')
parser.add_argument("inputWindx", type=str, help='input windx .prb filename')
parser.add_argument("inputWindy", type=str, help='input windy .prb filename')
parser.add_argument("inputObs", type=str, help='input obs .prb filename')
parser.add_argument("-o", "--output", type=str, help='output basename')

args = parser.parse_args()
print "input filename is {}".format(args.input)

if args.output:
    print "output filename(s) is(are) {}XX.noos".format(args.output)
    
if __name__ == "__main__":
    if args.output:
        prb2noos(args.input,args.output,args.inputWindx,args.inputWindy,args.inputObs)
    else:
        prb2noos(args.input,args.input,args.inputWindx,args.inputWindy,args.inputObs)
        


