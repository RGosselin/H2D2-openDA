# OpenDA - H2D2 wrapper

This *java* wrapper is used by [OpenDA](https://www.openda.org/) in its compiled form to be able
to communicate with *H2D2* and allow for automatic parameter calibration and data assimilation.

## Getting Started

Detailed instruction on how to use the wrapper are given in the `H2D2-oDA_userguide.pdf` userguide provided at the
root of this git. This README file contains information on how to compile a new version of the *OpenDA* wrapper.

## Prerequisites

A working installation of *Ant Apache* is required for compilation.
A complete installation of *OpenDA* is not required for the compilation of the wrapper,
but a `jar` from the OpenDA installation folder is needed (`openda_core.jar`). 
The present wrapper have been tested with *OpenDA 2.4*, and the `.jar` from this version
is provided in the `bin_external` folder. Other versions of *OpenDA* have not been tested.

## Wrapper compilation

In a windows environment, simply launch the `compilation.bat` batch file. It reads
the source code in the `source` folder, and places the compiled file in the `bin` folder.
In other environments, simply launch the command `ant build` in a command line / terminal.

Documentation can be generated using the `ant doc` command.

## Deployment

To be able to use the OpenDA_H2D2 wrapper, the compiled `jar` file (`H2D2_oDA.jar`)
must be placed in the `bin` folder of the OpenDA installation




