#!/usr/bin/env python
"""Reads and converts a single .noos files to a H2D2 .prgl file.
Input arguments are:
    - input filename
    - output filename
    - parameter lower limit
    - parameter upper limit
    - OpenDA standard deviation (noise)
Date format in the .noos file is YYYYmmddHHMM
Please modify the script to change fixed prgl values and select which prgl
value is replaced by the one from the .noos file.
"""
# -*- coding: utf-8 -*-

import time
import calendar
import math
import argparse

def noos2prgl(inputName, outputName, prgl, L1, L2, sigma):
    # Create empty date and value lists
    prglDate = []
    prglValue = []

    # Open .noos input file
    with open(inputName, 'rt') as ifs:
        while True:
            # Loop on all lines
            line = ifs.readline()
            # If line does not exist, exit loop
            if not line:
                break
            else:
                # If the first character is not #
                if line[0] != '#':
                    # Read line splits
                    dateTemp, valueTemp = line.split()
                    # Convert YYYYmmddHHMM to Python time
                    dateTime = time.strptime(dateTemp, "%Y%m%d%H%M")
                    # Convert time to epoch
                    dateEpoch = calendar.timegm(dateTime)
                    # Store epoch time
                    prglDate.append(dateEpoch)
                    # Apply error function to the value using standard
                    # deviation (sigma) and lower and upper limits (L1, L2)
                    valueTempCDF = (0.5 * (1 +
                                           math.erf(float(valueTemp)
                                                    /(sigma*math.sqrt(2)))))
                    # Store value in the value list
                    prglValue.append((L2-L1)*valueTempCDF+L1)

    # Open output file
    with open(outputName, 'wt') as ofs:
        # Loop on all dates
        for iy in range(len(prglDate)):
            # Write time header - 1 (number of columns), number of values, date
            ofs.write('%i %i %15.5f\n' % (1, len(prgl), prglDate[iy]))

            # Loop on prgl values
            for iz in range(0, len(prgl)):
                # If the id is wind, replace with value from .noos file
                if prgl[iz] == 'wind':
                    ofs.write('%9.6e ' % (prglValue[iy]))
                # Otherwise write the value from main()
                else:
                    ofs.write('%9.6e ' % prgl[iz])
            ofs.write('\n')

    print('Conversion completed!')

def main():
    """Reads and converts a single .noos files to a H2D2 .prgl file.
    Input arguments are:
        - input filename
        - output filename
        - parameter lower limit
        - parameter upper limit
        - OpenDA standard deviation (noise)
    Date format in the .noos file is YYYYmmddHHMM
    Please modify the script to change fixed prgl values and select which prgl
    value is replaced by the one from the .noos file.
    """
    parser = argparse.ArgumentParser(description='Reads and converts a .noos \
                                     file to H2D2 prgl file.')
    parser.add_argument("input", type=str, help='input filename')
    parser.add_argument("output", type=str, help='output filename')
    parser.add_argument("L1", type=float, help='parameter low limit')
    parser.add_argument("L2", type=float, help='parameter high limit')
    parser.add_argument("sigma", type=float, help='OpenDA standard deviation')

    args = parser.parse_args()

    prgl = []

    prgl.append('9.8059999999999992e+000')                #  1 gravity
    prgl.append('4.2000000000000000e+001')                #  2 latitude
    prgl.append('10')                                     #  3 cw formula ! 6 - Kumar old waves / -1 - Woo relative
    prgl.append('1.0000000000000000e-006')                #  4 laminar viscosity
    prgl.append('1.0000000000000000e+000')                #  5 mixing length coef
    prgl.append('0.0000000000000000e+000')                #  6 mixing length coef related to grid size
    prgl.append('1.0000000000000000e-006')                #  7 viscosity limiter: inf
    prgl.append('1.0000000000000000e+002')                #  8 viscosity limiter: sup
    prgl.append('0.2000000000000000e-000')                #  9 dry/wet: threshold depth
    prgl.append('0.1000000000000000e-000')                # 10 dry/wet: min depth
    prgl.append('0.5000000000000000e+000')                # 11 dry/wet: manning
    prgl.append('1.0000000000000000e-000')                # 12 dry/wet: |u| max
    prgl.append('1.0000000000000000e+000')                # 13 dry/wet: porosity
    prgl.append('0.0000000000000000e+000')                # 14 dry/wet: damping
    prgl.append('1.0')                                    # 15 dry/wet: coef. convection
    prgl.append('1.0')                                    # 16 dry/wet: coef. gravity
    prgl.append('1.0e-003')                               # 17 dry/wet: nu visco
    prgl.append('0.5')                                    # 18 dry/wet: Peclet
    prgl.append('1.0')                                    # 19 dry/wet: nu darcy
    prgl.append('1.2000000000000000e+000')                # 20 peclet           -> 1.2
    prgl.append('0.0e-004')                               # 21 damping coef.    -> 0.0
    prgl.append('0.0e-000')                               # 22 free surface smoothing (Darcy)
    prgl.append('0.2500000000000000e-005')                # 23 free surface smoothing (Lapidus)
    prgl.append('1.0')                                    # 24 coef. convection
    prgl.append('1.0')                                    # 25 coef. gravity
    prgl.append('1.0')                                    # 26 coef. manning
    prgl.append('wind')                                   # 27 coef. wind
    prgl.append('1.0')                                    # 28 coef. boundary integral
    prgl.append('1.0e-018')                               # 29 coef. penality
    prgl.append('1.0e-007')                               # 30 coef. Kt perturbation (~ 0.1*sqrt(eps_mach); eps_mach ~ 2.2e-16)
    prgl.append('1.0e-012')                               # 31 min   Kt perturbation (~ 0.1*sqrt(eps_mach); eps_mach ~ 2.2e-16)
    prgl.append('1.0')                                    # 32 coef. Kt relaxation
    prgl.append('1')                                      # 33 flag prel perturbation (0 = false, #=0 = true)
    prgl.append('1')                                      # 34 flag prno perturbation (0 = false, #=0 = true)

    noos2prgl(args.input, args.output, prgl, args.L1, args.L2, args.sigma)

    print("output filename is {}".format(args.output))

if __name__ == "__main__":
    main()
