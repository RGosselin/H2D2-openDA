#-------------------------------------------------------------------
# Summary:  Build the sv2d simulation data
#
# Description:
#     The function build_solution_sv2d returns a handle on a newly created
#     simulation data for a Saint-Venant problem.
#
# Input:
#     h_grid         Handle on the grid
#     h_prgl         Handle on the global properties
#     mnt_dir        Directory containing all the file for the MNT
#     mnh_dir        Directory containing all the file for the MNT
#
# Output:
#     Handle on the constructed simd
#
# Notes:
#-------------------------------------------------------------------
function build_solution_sv2d(h_grid, h_prgl, sim_dir, dyn_dir, stat_dir, name_cnd, name_bnd, l_gmres)
    assert_symbol(h_grid, 'build_solution_sv2d: Undefined symbol: h_grid')
    assert_symbol(h_prgl, 'build_solution_sv2d: Undefined symbol: h_prgl')
    assert_symbol(dyn_dir,'build_solution_sv2d: Undefined symbol: dyn_dir')
    assert_symbol(stat_dir,'build_solution_sv2d: Undefined symbol: stat_dir')
    assert_symbol(name_cnd,'build_solution_sv2d: Undefined symbol: name_cnd')
    assert_symbol(name_bnd,'build_solution_sv2d: Undefined symbol: name_bnd')
    #assert_symbol(h_ddl,'build_solution_sv2d: Undefined symbol: h_ddl')

    #---  MNT - prno
    h_prn = build_prno_sv2d(dyn_dir, stat_dir)  #Propri�t� nodales

    #---  Scenario - bc
    h_bc  = build_bc(sim_dir, stat_dir, name_cnd, name_bnd)

    #---  Degree of freedom (unknowns)
    h_ddl = dof()

    #---  Elemental properties
    h_pre = 0

    #---  Sollicitations
    h_slc = 0                    # No concentrated sollicitations
    h_slr = 0                    # No spatial sollicitations

    #---  Global simulation data
    if (l_gmres == 1)
        h_frm = form('SV2D_Conservatif_CDYS_NN')       # Formulation - dof bloc numbering
    else
        h_frm = form('SV2D_Conservatif_CDY2_NN')       # Formulation - dof node numbering
    endif
    #h_sol = simd(h_frm, h_grid, h_ddl, h_bc, h_slc, h_slr, h_prgl, h_prn, h_pre)
    h_sol = sv2d_y4(h_grid, h_ddl, h_bc, h_slc, h_slr, h_prgl, h_prn, h_pre)
    
    build_solution_sv2d = h_sol
endfunction







#-------------------------------------------------------------------
# Summary: Builds the sv2d algorithm sequence item
#
# Description:
#     The function build_sequence_sv2d returns a handle on a newly created
#     sequence item for a Saint-Venant problem.
#
# Input:
#     h_simd      Handle on the simulation data
#     h_slvr      Handle on the solver
#     sim_dir     Directory containing the results file.
#
# Output:
#     Handle on the algorithm sequence item
#
# Notes:
#-------------------------------------------------------------------
function build_item_algo_sv2d(h_simd, h_slvr)

   assert_symbol(h_simd, 'build_sequence_sv2d: Undefined symbol: h_simd')
   assert_symbol(h_slvr, 'build_sequence_sv2d: Undefined symbol: h_slvr')

   #---  Sequence Item
   h_item = algo_sequence_algo(h_simd, h_slvr)

   build_item_algo_sv2d = h_item
endfunction



#-------------------------------------------------------------------
# Summary: Builds the sv2d post-treatment sequence item
#
# Description:
#     The function build_sequence_sv2d returns a handle on a newly created
#     post-treatment sequence item for a Saint-Venant problem.
#
# Input:
#     h_simd      Handle on the simulation data
#     h_slvr      Handle on the solver
#     sim_dir     Directory containing the results file.
#
# Output:
#     Handle on the post-treatment sequence item
#
# Notes:
#-------------------------------------------------------------------
function build_item_post_sv2d(h_simd, sim_dir)

   assert_symbol(h_simd, 'build_sequence_sv2d: Undefined symbol: h_simd')
   assert_symbol(sim_dir,'build_sequence_sv2d: Undefined symbol: sim_dir')

   #---  Post-treatment during resolution
   h_pst_sim = build_pstd_sv2d  (h_simd,sim_dir)
   
   #---  Add to sequence
   h_trg = trigger_integer(1)
   h_itm = algo_sequence_post(h_simd, h_pst_sim, h_trg)

   build_item_post_sv2d = h_itm
endfunction




 
 
 




















