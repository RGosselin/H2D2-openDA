#!/usr/bin/env python
"""Write the association file depending on the location of the node
relative to the wkt objects, and the buffer value.
If a node is located inside a wkt object and more than the buffer length
away from its boundaries, its association value to this zone is 1. All
remaining zones are skipped.
If the node is less than the buffer length away from a zone boundary, its
association value with this zone varies depending on the distance to the
boundary. A node on the boundary has an association value of 0.5, and a node
+/- 1 buffer length away from the boundary has an association value of 1
or 0, depending if it is located inside or ouside the object. An error
function is used between the two. Subsequent objects are not skipped,
meaning a node could have non-zero associations to multiple zones.
"""
# -*- coding: utf-8 -*-

import argparse
import math
import shapely.geometry
import shapely.wkt

def read_header(file):
    """Simple function to read the header of a H2D2 data file.
    Input is an open Python file.
    Returns the tuple (number of nodes, number of columns)
    """
    # Read first line of the input file
    line = file.readline()
    # Extract the number of nodes (1st number) and number of columns (2nd number)
    nbNodes, nbCol, _ = line.strip().split(None, 2)
    # Convert strings to integers
    nbNodes = int(nbNodes)
    nbCol = int(nbCol)
    # Return the value
    return (nbNodes, nbCol)

def read_vnod(vnodFile):
    """Read H2D2 data files and stores the values in the vnod list.
    Each column in the data file is a sublist of vnod
    """
    # Open input file
    with open(vnodFile, 'rt') as afs:
        # Call header function to extract the number of columns
        header = read_header(afs)
        nbColumns = header[1]
        # Create empty nodal values list
        vnod = []
        # Create sublists - one for each column
        for i in range(0, nbColumns):
            vnod.append([])
        # Store nodal values in vnod
        for line in afs:
            value = line.strip().split(' ')
            # Loop on columns
            for i in range(0, nbColumns):
                vnod[i].append(float(value[i]))
    # Return vnod
    return vnod

def read_wkt(wktFilename):
    """Read a wkt file.
    The header of the file must have 3 parts, and the third one must be
    WKT (in capital letters). The first and second one are not restricted but
    must exist. Suggested header is TYPE ID WKT
    Each subsequent line is comprised of the type, the id, and a wkt object
    (point, linestring, polygon, multipolygon, etc.)
    This function returns a list with 3 sublists: (type, id, object)
    """
    # Open .wkt file
    with open(wktFilename, 'rt') as bfs:
        # Store first line as header
        header = bfs.readline()
        _, _, hdrWKT = header.strip().split(' ', 2)
        # Check if the third element of the header is WKT. If not, exit.
        if hdrWKT != 'WKT':
            print('WKT file header must be TYPE ID WKT. Please check input file')
            quit()
        # Create empty lists for type, id and objects
        typeStore = []
        idStore = []
        wktStore = []
        # Loop on the remaining lines
        for line in bfs:
            # Split values
            wktType, wktId, wkt = line.strip().split(' ', 2)
            # Store data
            typeStore.append(wktType)
            idStore.append(wktId)
            wktStore.append(wkt)
    # Return type, id and objects in a list of lists
    return (typeStore, idStore, wktStore)

def compute_zne(vnod, wktBundle, outputfile, bufferString):
    """Write the association file depending on the location of the node
    relative to the wkt objects, and the buffer value.
    If a node is located inside a wkt object and more than the buffer length
    away from its boundaries, its association value to this zone is 1. All
    remaining zones are skipped.
    If the node is less than the buffer length away from a zone boundary, its
    association value with this zone varies depending on the distance to the
    boundary. A node on the boundary has an association value of 0.5, and a node
    +/- 1 buffer length away from the boundary has an association value of 1
    or 0, depending if it is located inside or ouside the object. An error
    function is used between the two. Subsequent objects are not skipped,
    meaning a node could have non-zero associations to multiple zones.
    """
    # Convert buffer string to float
    buffer = float(bufferString)
    # Extract number of zones from the number of objects in the wkt file
    nbZones = len(wktBundle[0])
    # Extract number of nodes from the coordinates file
    nbNodes = len(vnod[0])
    # Extract coordinates dimensions from the coordinates files
    dimVnod = len(vnod)
    # Standard deviation for the error function. 3*sigma => 0.9973 value
    sigma = buffer/3

    # Create empty zone list
    zone = []

    # Fill zone list with shapely objects
    for i in range(0, nbZones):
        zone.append(shapely.wkt.loads(wktBundle[2][i]))
        print(('%s %s' % (wktBundle[0][i], wktBundle[1][i])))
        print(zone[i])

    print(('Number of nodes = %i' % nbNodes))
    print(('Coordinates - number of dimensions = %i' % dimVnod))
    print(('Number of zones = %i' % nbZones))

    # Open output file
    with open(outputfile, 'wt') as ofs:
        # Write header (H2D2 data format)
        ofs.write('%i %i 0.0\n' % (nbNodes, nbZones))

        # Create empty association list with nbZones elements
        assocZne = []
        for k in range(0, nbZones):
            assocZne.append(0)

        # Loop on all nodes
        for i in range(0, nbNodes):
            # loop on all zones to reset association values to 0
            for k in range(0, nbZones):
                assocZne[k] = 0
            # Loop on all zones to determine whether a point is within a zone
            # (contained) and its distance to the boundaries
            for j in range(0, nbZones):
                # Create shapely object for the node - 2D or 3D
                if dimVnod == 2:
                    node = shapely.geometry.Point(vnod[0][i], vnod[1][i])
                elif dimVnod == 3:
                    node = shapely.geometry.Point(vnod[0][i],
                                                  vnod[1][i],
                                                  vnod[2][i])
                else:
                    print('Error : nodes coordinates are neither 2D nor 3D')
                    exit()
                # Compute distance to boundary
                distance = zone[j].exterior.distance(node)
                # Check whether the zone contained the node
                contained = node.within(zone[j])

                # If node is in the zone
                if contained:
                    # Check if boundary distance is greater than the buffer
                    if distance > buffer:
                        # If yes, association is 1, and break the zone loop
                        assocZne[j] = 1
                        break
                    else:
                        # Else, association value is function of the distance
                        # and between 0.5 and 1.0
                        assocZne[j] = 0.5 * (1 +
                                             math.erf(distance
                                                      /(sigma*math.sqrt(2))))
                else:
                    # If node is not in the zone but less than a buffer length
                    # from its boundaries, association is function of the
                    # distance and between 0 and 0.5
                    if distance < buffer:
                        assocZne[j] = 1-(0.5 * (1 +
                                                math.erf(distance
                                                         /(sigma*math.sqrt(2)))))

            # Loop on all zones to write the association values
            for j in range(0, nbZones):
                ofs.write('%4.3f ' % assocZne[j])
            ofs.write('\n')

def main():
    """Write the association file depending on the location of the node
    relative to the wkt objects, and the buffer value.
    If a node is located inside a wkt object and more than the buffer length
    away from its boundaries, its association value to this zone is 1. All
    remaining zones are skipped.
    If the node is less than the buffer length away from a zone boundary, its
    association value with this zone varies depending on the distance to the
    boundary. A node on the boundary has an association value of 0.5, and a node
    +/- 1 buffer length away from the boundary has an association value of 1
    or 0, depending if it is located inside or ouside the object. An error
    function is used between the two. Subsequent objects are not skipped,
    meaning a node could have non-zero associations to multiple zones.

    Input arguments are:
        - Coordinates filename
        - Output filename (association file)
        - zone definition in WKT format
        - Boundary buffer value
    """
    parser = argparse.ArgumentParser(description='Reads %1 original nodal \
                                     values file, write %2 zone association \
                                     file')
    parser.add_argument("orig_file",
                        type=str,
                        help='original nodal values filename')
    parser.add_argument("asso_file",
                        type=str,
                        help='zone association filename')
    parser.add_argument("wkt_file",
                        type=str,
                        help='Zone wkt definition file')
    parser.add_argument("buffer",
                        type=str,
                        help='Zone buffer value')

    args = parser.parse_args()

    # Extract coordinates from input file
    vnod = read_vnod(args.orig_file)
    # Extract WKT objects from wkt file
    wktBundle = read_wkt(args.wkt_file)
    # Compute association values for each node
    compute_zne(vnod, wktBundle, args.asso_file, args.buffer)

if __name__ == '__main__':
    main()
    