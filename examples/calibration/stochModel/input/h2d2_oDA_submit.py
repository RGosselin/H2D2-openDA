#!/usr/bin/env python
"""Script used to submit H2D2 tasks from OpenDA.
A log directory is created and given to H2D2 as the log_dir variable.
Input H2D2 filename is command.inp
Output H2D2 filename is output_date.out, with date the submission time
If the computing platform is Linux based (Lachine cluster), the script checks
every 60 seconds is the error file as been written in order to keep the
Python script open while H2D2 is computing.
This check is not necessary on Windows platforms (untested - experimental)
"""
# -*- coding: UTF-8 -*-
from sys import platform as _platform
import subprocess
import os.path
import os
import time
from datetime import datetime

currentTime = datetime.now()

# Create a log directory
directory = 'log' + currentTime.strftime('%Y%m%d-%H%M') + '/'
# Compute H2D2 argument list
h2d2Arguments = ('-fi=command.inp -fo=output_' +
                 currentTime.strftime('%Y%m%d-%H%M') +
                 '.out log_dir=\'' + directory + '\'')

# If the log directory does not exist, create it
if not os.path.exists(directory):
    os.makedirs(directory)

# If the paltform is Linux
if _platform == "linux" or _platform == "linux2":
    # Submit the subprocess
    submit2 = subprocess.check_output(['soumet.sh',
                                       '--c=1x1x8 h2d2 ' + h2d2Arguments])
    # Check the output to store the jobID
    submit = submit2.decode()
    print(submit)

    # Extract the jobID from the output
    _, l2 = submit.strip().split('\n')
    jobId, _ = l2.split('.', 1)
    print('job ID {}'.format(jobId))

    # Reset minutes counter
    n = 0
    # Reset end toggle
    end = False
    # error filename, only written when the jobs stops
    filename = '__soumet_qsub.sh.e%s' % jobId
    # While the toggle is false
    while not end:
        # Check if the error file exists
        if os.path.exists(filename):
            # Display the number of minutes
            print(('Total computation time: ' + str(n) + ' minutes'))
            # Switch the end toggle to True
            end = True
        # Otherwise wait 60s and concatenate the minutes counter
        else:
            time.sleep(60)
            n += 1

# Else if the platform is Windows
elif _platform == "win64" or _platform == "win32":
    # Call H2D2
    subprocess.call('C:/Program Files/H2D2/bin/h2d2.bat ' + h2d2Arguments)
