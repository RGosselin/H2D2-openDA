#-------------------------------------------------------------------
# Summary:
#
# Description:
#   The function build_mesh
#
# Input:
#     mesh_dir        Directory containing all necessary file to construct the mesh.
#     mnh_dir         Directory containing all necessary file to construct the mnh.
#     mnt_sv2d_dir    Directory containing all necessary file to construct the mnt
#                     for convection-diffusion problem.
#     h_pgl_sv2d      Handle on the global properties for convection-diffusion problem.
#     nb_proc         number pf processor.
#
# Output:
#     h_sol_cd2d      Handle on the constructed simd
#
# Notes:
#-------------------------------------------------------------------
function build_solution_cd2d(h_grid, h_prgl, dyn_dir, stat_dir, h_vno_lnk, name_cnd, name_bnd)

    assert_symbol(h_grid,   'build_solution_cd2d: Undefined symbol: h_grid')
    assert_symbol(h_prgl,   'build_solution_cd2d: Undefined symbol: h_prgl')
    assert_symbol(stat_dir,  'build_solution_cd2d: Undefined symbol: stat_dir')
    assert_symbol(h_vno_lnk,'build_solution_cd2d: Undefined symbol: h_vno_lnk')

    #---  MNT_link - prno
    h_prn = build_prno_cd2d_from_sv2d(h_vno_lnk)

    #---  Scenario - bc
    h_bc  = build_bc_cd2d(stat_dir,name_cnd,name_bnd)

    #---  Degree of freedom (unknowns)
    h_ddl  = dof()

    #---  Elemental properties
    h_pre = 0

    #---  Sollicitations
    h_slc = 0                    # No concentrated sollicitations
    h_slr = 0                    # No spatial sollicitations

    #---  Global simulation data

    h_frm = form('cd2d_conservatif')       # Formulation - dof node numbering
    h_sol = simd(h_frm, h_grid, h_ddl, h_bc, h_slc, h_slr, h_prgl, h_prn, h_pre)
    build_solution_cd2d = h_sol
endfunction

#-------------------------------------------------------------------
# Summary:
#
# Description:
#   The function build_sequence_cd2d
#
# Input:
#     h_sol_cd2d        Handle on the constructed simd for convection-diffusion problem.
#     h_seq             Handle on the constructed algo_sequence.
#     h_solver_cd2d     Handle on the solver for convection-diffusion(strategy)
#     sim_dir           Handle on the global properties for Saint Venant problem.
#     mesh_dir          Directory containing all necessary file to construct the mesh
#
# Output:
#
# Notes:
#-------------------------------------------------------------------
function build_item_algo_cd2d(h_simd, h_slvr, sim_dir)   
    assert_symbol(h_simd, 'build_item_algo_cd2d: Undefined symbol: h_simd')
    assert_symbol(h_slvr, 'build_item_algo_cd2d: Undefined symbol: h_slvr')
    assert_symbol(sim_dir,'build_item_algo_cd2d: Undefined symbol: sim_dir')

    #---  Sequence Item
    h_item = algo_sequence_algo(h_simd, h_slvr)

    build_item_algo_cd2d = h_item
endfunction 

#-------------------------------------------------------------------
# Summary:
#
# Description:
#   The function build_sequence_cd2d
#
# Input:
#     h_sol_cd2d        Handle on the constructed simd for convection-diffusion problem.
#     h_seq             Handle on the constructed algo_sequence.
#     h_solver_cd2d     Handle on the solver for convection-diffusion(strategy)
#     sim_dir           Handle on the global properties for Saint Venant problem.
#     mesh_dir          Directory containing all necessary file to construct the mesh
#
# Output:
#
# Notes:
#-------------------------------------------------------------------
function build_item_post_cd2d(h_simd, out_dir)
    assert_symbol(h_simd, 'build_item_algo_cd2d: Undefined symbol: h_simd')
    assert_symbol(out_dir,'build_item_algo_cd2d: Undefined symbol: out_dir')

    #---  Post-treatment during resolution
    h_pst_sim = build_pstd_cd2d(out_dir)


    h_trg = trigger_integer(1)
    h_itm = algo_sequence_post(h_simd, h_pst_sim, h_trg)

    build_item_post_cd2d = h_itm
endfunction





#-------------------------------------------------------------------
# Summary:
#
# Description:
#   The function build_mesh
#
# Input:
#     mesh_dir        Directory containing all necessary file to construct the mesh.
#     mnh_dir         Directory containing all necessary file to construct the mnh.
#     mnt_sv2d_dir    Directory containing all necessary file to construct the mnt
#                     for convection-diffusion problem.
#     h_pgl_sv2d      Handle on the global properties for convection-diffusion problem.
#     nb_proc         number pf processor.
#
# Output:
#     h_sol_cd2d      Handle on the constructed simd
#
# Notes:
#-------------------------------------------------------------------
function build_solution_cd2d_2(h_grid, h_prgl, mnh_dir,mnt_dir,name_cnd,name_bnd)
    assert_symbol(h_grid,   'build_solution_cd2d: Undefined symbol: h_grid')
    assert_symbol(h_prgl,   'build_solution_cd2d: Undefined symbol: h_prgl')
    assert_symbol(mnh_dir,  'build_solution_cd2d: Undefined symbol: mnh_dir')

    #---  MNT_link - prno
    h_prn = build_prno_cd2d_2(mnt_dir)

    #---  Scenario - bc
    #---  Scenario - bc
    h_bc  = build_bc(mnh_dir,name_cnd,name_bnd)

    #---  Degree of freedom (unknowns)
    h_ddl  = dof()

    #---  Elemental properties
    h_pre = 0

    #---  Sollicitations
    h_slc = 0                    # No concentrated sollicitations
    h_slr = 0                    # No spatial sollicitations

    #---  Global simulation data
    h_frm = form('cd2d_conservatif')       # Formulation - dof node numbering
    h_sol = simd(h_frm, h_grid, h_ddl, h_bc, h_slc, h_slr, h_prgl, h_prn, h_pre)
    build_solution_cd2d_2 = h_sol
endfunction
